/**
 * This file is part of Scale Connector.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 * 
 * Copyright (C) 2015 INGEINT <http://www.ingeint.com>.
 * Copyright (C) Contributors.
 * 
 * Contributors:
 *    - 2015 Saúl Piña <spina@ingeint.com>.
 */

package com.ingeint.info;

public class SCPluginFeatures {

	public static final String rootPackage = "com.ingeint";
	public static final String modelPackage = rootPackage + ".model";

	public static final String entityType = "IGI";
	public static final String prefixTable = "SC_";
	public static final String prefixModel = "M";
	public static final String prefixModelDefault = "X_";

	public static final String id = "com.ingeint.scaleconnector";
	public static final String name = "Scale Connector Plugin";
	public static final String vendor = "INGEINT";
	public static final String web = "http://ingeint.com";

	public static final String version = "3.0.0";

}
