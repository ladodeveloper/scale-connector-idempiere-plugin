/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/

package ve.net.dcs.sc.idempiere.process;

import java.io.IOException;
import java.net.SocketTimeoutException;

import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;

import ve.net.dcs.sc.component.Client;
import ve.net.dcs.sc.component.Request;
import ve.net.dcs.sc.component.RequestType;
import ve.net.dcs.sc.component.Response;
import ve.net.dcs.sc.idempiere.model.MSCServerSettings;
import ve.net.dcs.sc.idempiere.util.HelperDate;

/**
 * Process to test the connection to the server
 * 
 * @author Double Click Sistemas C.A. - http://dcs.net.ve
 * @author Saul Pina - spina@dcs.net.ve
 */
public class ScaleServerTest extends SvrProcess {

	private MSCServerSettings record;

	public ScaleServerTest() {
	}

	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();

		for (ProcessInfoParameter p : para) {
			String name = p.getParameterName();
			if (name.equals("SC_ServerSettings_ID"))
				record = new MSCServerSettings(getCtx(), p.getParameterAsInt(), get_TrxName());
			else
				log.severe("Unknown Parameter: " + name);
		}
	}

	@Override
	protected String doIt() throws Exception {
		try {
			return connectServerSC();
		} catch (SocketTimeoutException e) {
			throw new AdempiereUserError("Finish timeout", e);
		} catch (Exception e) {
			throw new AdempiereUserError("Error connecting to server", e);
		}
	}

	/**
	 * Sends a request to the server, the request must contain all information
	 * necessary to obtain the desired response
	 * 
	 * @return The response obtained
	 * @throws SocketTimeoutException
	 *             If timeout expires before connecting
	 * @throws IOException
	 *             If an error occurred when reading from the input stream
	 * @throws ClassNotFoundException
	 *             If the returned object is of a class unknown
	 */
	public String connectServerSC() throws SocketTimeoutException, ClassNotFoundException, IOException {
		Client client = new Client(record.getHostAddress(), record.getHostPort());
		client.setTimeout(record.getSecondsTimeout() * 1000);
		client.setWebService(record.isWebService());
		Request request = new Request();
		request.setType(RequestType.TEST);
		request.setDate(HelperDate.now());
		Response response = client.sendRequest(request);
		return response.getStatusNotice() + ", Server time: " + HelperDate.format(response.getDate(), "H:mm:ss yyyy-MM-dd");
	}

}
