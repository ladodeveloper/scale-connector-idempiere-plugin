package ve.net.dcs.sc.ui.util;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JTextField;

/**
 * @author Saul Pina - spina@dcs.net.ve
 */
public class JIntegerField extends JTextField {

	private static final long serialVersionUID = -3669579723172144525L;

	public JIntegerField() {
		super();
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (!Character.isDigit(e.getKeyChar())) {
					e.consume();
				}
			}
		});
	}

	public int getInteger() {
		return Integer.parseInt(getText().trim());
	}

	public void setInteger(int i) {
		setText(String.valueOf(i));
	}

}
