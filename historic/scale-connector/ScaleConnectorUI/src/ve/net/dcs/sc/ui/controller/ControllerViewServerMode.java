/**
 * 
 * Scale Connector
 * 
 * Copyright (C) Double Click Sistemas C.A. RIF: J-31576020-7 
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Double Click Sistemas C.A. Barquisimeto, Venezuela, http://dcs.net.ve
 * 
 */

package ve.net.dcs.sc.ui.controller;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import jssc.SerialPortList;
import jssc.SerialPortTimeoutException;
import ve.net.dcs.sc.component.ScaleConnector;
import ve.net.dcs.sc.component.Server;
import ve.net.dcs.sc.ui.feature.SCUIFeature;
import ve.net.dcs.sc.ui.feature.SCUILocale;
import ve.net.dcs.sc.ui.util.HelperFile;
import ve.net.dcs.sc.ui.view.ViewAbout;
import ve.net.dcs.sc.ui.view.ViewServerMode;
import ve.net.dcs.sc.ui.view.ViewWait;

/**
 * @author Double Click Sistemas C.A. - http://dcs.net.ve
 * @author Saul Pina - spina@dcs.net.ve
 */
public class ControllerViewServerMode implements ActionListener, WindowListener {

	private ViewServerMode viewServerMode;
	private Server server;
	private ArrayList<JMenuItem> manual;
	private ViewWait viewWait;

	private static Logger logger = Logger.getLogger(ControllerViewServerMode.class.getName());

	public ControllerViewServerMode() {
		viewServerMode = new ViewServerMode();
		viewServerMode.addListener(this);
		initView();
		viewServerMode.setVisible(true);
		viewServerMode.setResizable(false);
	}

	public void initView() {
		for (String portName : SerialPortList.getPortNames()) {
			viewServerMode.getCmbModelSerialPort().addElement(portName);
		}

		if (viewServerMode.getCmbModelSerialPort().getIndexOf(SCUIFeature.get("DEFAULT_SERIALPORT")) > 0) {
			viewServerMode.getCmbModelSerialPort().setSelectedItem(SCUIFeature.get("DEFAULT_SERIALPORT"));
		}

		viewServerMode.getTxtBaud().setText(SCUIFeature.get("DEFAULT_BAUD"));
		viewServerMode.getTxtDataBits().setText(SCUIFeature.get("DEFAULT_DATABITS"));
		viewServerMode.getTxtParity().setText(SCUIFeature.get("DEFAULT_PARITY"));
		viewServerMode.getTxtStopBits().setText(SCUIFeature.get("DEFAULT_STOPBITS"));
		viewServerMode.getTxtPort().setText(SCUIFeature.get("DEFAULT_PORT"));
		viewServerMode.getTxtByteCount().setText(SCUIFeature.get("DEFAULT_BYTECOUNT"));
		viewServerMode.getTxtStartCharacter().setText(SCUIFeature.get("DEFAULT_STARTCHARACTER"));
		viewServerMode.getTxtEndCharacter().setText(SCUIFeature.get("DEFAULT_ENDCHARACTER"));
		viewServerMode.getTxtReadings().setText(SCUIFeature.get("DEFAULT_READINGS"));
		viewServerMode.getBtnStop().setEnabled(false);

		File[] files = HelperFile.files(SCUIFeature.get("PATH_MANUAL"));
		manual = new ArrayList<JMenuItem>();
		for (int i = 0; files != null && i < files.length; i++) {
			JMenuItem item = new JMenuItem(files[i].getName());
			item.addActionListener(this);
			viewServerMode.getMenuItemManual().add(item);
			manual.add(item);
		}

		viewServerMode.getTblModelBytesRead().setColumnIdentifiers(new Object[] { "POS", "CHAR", "INT" });

	}

	public void close() {
		if (server != null && server.isAlive()) {
			int option = JOptionPane.showConfirmDialog(viewServerMode, SCUILocale.get("ViewServerMode.changeMode"), SCUILocale.get("ViewClientMode.menuItemChangeMode"), JOptionPane.OK_CANCEL_OPTION);
			if (option != JOptionPane.OK_OPTION) {
				return;
			}
		}
		viewServerMode.dispose();
		System.exit(0);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(viewServerMode.getMenuItemClose())) {
			close();
		} else if (e.getSource().equals(viewServerMode.getMenuItemAbout())) {
			new ViewAbout().setVisible(true);
		} else if (e.getSource().equals(viewServerMode.getMenuItemChangeMode())) {
			changeMode();
		} else if (e.getSource().equals(viewServerMode.getBtnStart())) {
			startServer();
		} else if (e.getSource().equals(viewServerMode.getBtnStop())) {
			stopServer();
		} else if (e.getSource().equals(viewServerMode.getBtnReadPort())) {
			readPort();
		} else {
			for (JMenuItem item : manual) {
				if (e.getSource().equals(item)) {
					try {
						Desktop.getDesktop().open(new File(SCUIFeature.get("PATH_MANUAL") + item.getText()));
					} catch (Exception e1) {
						JOptionPane.showMessageDialog(viewServerMode, SCUILocale.get("ViewClientMode.notOpen"), "Error", JOptionPane.ERROR_MESSAGE);
						e1.printStackTrace();
					}
				}
			}
		}
	}

	private void changeMode() {
		if (server != null && server.isAlive()) {
			int option = JOptionPane.showConfirmDialog(viewServerMode, SCUILocale.get("ViewServerMode.changeMode"), SCUILocale.get("ViewClientMode.menuItemChangeMode"), JOptionPane.OK_CANCEL_OPTION);
			if (option != JOptionPane.OK_OPTION) {
				return;
			}
			stopServer();
		}

		viewServerMode.dispose();
		new ControllerViewSelectMode();

	}

	public synchronized void readPort() {
		viewWait = new ViewWait();

		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					viewServerMode.getTblModelBytesRead().getDataVector().removeAllElements();

					int baud = viewServerMode.getTxtBaud().getInteger();
					int dataBits = viewServerMode.getTxtDataBits().getInteger();
					int stopBits = viewServerMode.getTxtStopBits().getInteger();
					int parity = viewServerMode.getTxtParity().getInteger();
					int byteCount = viewServerMode.getTxtByteCount().getInteger();
					int readings = viewServerMode.getTxtReadings().getInteger();
					String port = viewServerMode.getCmbModelSerialPort().getSelectedItem().toString();

					SCUIFeature.set("DEFAULT_PARITY", viewServerMode.getTxtParity().getText().trim());
					SCUIFeature.set("DEFAULT_STOPBITS", viewServerMode.getTxtStopBits().getText().trim());
					SCUIFeature.set("DEFAULT_DATABITS", viewServerMode.getTxtDataBits().getText().trim());
					SCUIFeature.set("DEFAULT_BAUD", viewServerMode.getTxtBaud().getText().trim());
					SCUIFeature.set("DEFAULT_BYTECOUNT", viewServerMode.getTxtByteCount().getText().trim());
					SCUIFeature.set("DEFAULT_STARTCHARACTER", viewServerMode.getTxtStartCharacter().getText().trim());
					SCUIFeature.set("DEFAULT_ENDCHARACTER", viewServerMode.getTxtEndCharacter().getText().trim());
					SCUIFeature.set("DEFAULT_READINGS", viewServerMode.getTxtReadings().getText().trim());
					SCUIFeature.set("DEFAULT_SERIALPORT", port);

					try {
						SCUIFeature.save();
					} catch (Exception ex) {

					}

					ScaleConnector sc = new ScaleConnector(port, baud, dataBits, stopBits, parity);
					sc.setByteCount(byteCount);
					sc.setStartCharacter(viewServerMode.getTxtStartCharacter().getInteger());
					sc.setEndCharacter(viewServerMode.getTxtEndCharacter().getInteger());
					sc.setReadings(readings);

					byte[] bytesPortValue = sc.readBytes();
					// String portValue = sc.getValue(new String(bytesPortValue));
					String portValue = sc.readValue();

					viewServerMode.getLblPortValue().setText(portValue);

					for (int i = 0; i < bytesPortValue.length; i++) {
						viewServerMode.getTblModelBytesRead().addRow(new Object[] { i, (char) bytesPortValue[i], bytesPortValue[i] });
					}

				} catch (SerialPortTimeoutException e) {
					logger.severe("Finish timeout");
					JOptionPane.showMessageDialog(viewServerMode, SCUILocale.get("ViewServerMode.timeoutScaleConnector"), "Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				} catch (Exception e) {
					logger.severe("Error reading serial port");
					JOptionPane.showMessageDialog(viewServerMode, SCUILocale.get("ViewServerMode.errorReadingSerialPort"), "Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				} finally {
					viewWait.close();
				}

			}
		}).start();
		viewWait.display();

	}

	private synchronized void stopServer() {
		try {
			server.stop();
			viewServerMode.getBtnStop().setEnabled(false);
			viewServerMode.getBtnStart().setEnabled(true);
			viewServerMode.getLblStatus().setText(SCUILocale.get("ViewServerMode.lblStatusStop"));
		} catch (Exception e1) {
			logger.warning("Error stoping server");
			JOptionPane.showMessageDialog(viewServerMode, SCUILocale.get("ViewServerMode.errorStopingServer"), "Error", JOptionPane.ERROR_MESSAGE);
			e1.printStackTrace();
		}
	}

	private synchronized void startServer() {
		server = new Server(viewServerMode.getTxtPort().getInteger());
		SCUIFeature.set("DEFAULT_PORT", viewServerMode.getTxtPort().getText().trim());
		try {
			SCUIFeature.save();
		} catch (Exception ex) {

		}
		try {
			server.start();
			viewServerMode.getBtnStop().setEnabled(true);
			viewServerMode.getBtnStart().setEnabled(false);
			viewServerMode.getLblStatus().setText(SCUILocale.get("ViewServerMode.lblStatusStart"));
		} catch (Exception e1) {
			logger.warning("Error starting server");
			JOptionPane.showMessageDialog(viewServerMode, SCUILocale.get("ViewServerMode.errorStartingServer"), "Error", JOptionPane.ERROR_MESSAGE);
			e1.printStackTrace();
		}
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowClosed(WindowEvent e) {
	}

	@Override
	public void windowClosing(WindowEvent e) {
		close();
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowOpened(WindowEvent e) {
	}

}
