/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/

package ve.net.dcs.sc.idempiere.model;

import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import org.compiere.model.Query;

/**
 * Custom Model X_SC_ServerSettings
 * 
 * @author Double Click Sistemas C.A. - http://dcs.net.ve
 * @author Saul Pina - spina@dcs.net.ve
 */
public class MSCServerSettings extends X_SC_ServerSettings {

	private static final long serialVersionUID = -713397209151149360L;

	public MSCServerSettings(Properties ctx, int SC_ServerSettings_ID, String trxName) {
		super(ctx, SC_ServerSettings_ID, trxName);
	}

	public MSCServerSettings(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	protected boolean beforeDelete() {
		for (MSCScale line : getLines()) {
			line.deleteEx(true);
		}
		return true;
	}

	public List<MSCScale> getLines() {
		return new Query(getCtx(), I_SC_Scale.Table_Name, "SC_ServerSettings_ID=?", get_TrxName()).setParameters(get_ID()).list();
	}

}
