package ve.net.dcs.sc.test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ve.net.dcs.sc.component.ScaleConnector;


public class SCTest {

	public static void main(String[] args) {
		char sc = (char) 3;
		char ec = (char) 3;
		String patternString = String.format("%c([^%c%c]+)%c", sc, sc, ec, ec);
		String string = String.format("%c255%c%c255%c%c999%c", sc, ec,sc, ec,sc, ec);
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(string);
		
		
		
		HashMap<String, Integer> list = new HashMap<String, Integer>();
		
		while (matcher.find()) {
			String value = matcher.group(1).replaceAll("[^0-9]", "");
			if(list.containsKey(value)){
				list.put(value, list.get(value)+1);
			}else{
				list.put(value, 1);
			}
		} 
		
		Iterator<String> iterator = list.keySet().iterator();
		
		int max =0;
		String keyMax="";
		
		while (iterator.hasNext()) {
			String string2 = (String) iterator.next();
			if(list.get(string2)>max){
				max=list.get(string2);
				keyMax=string2;
			}
		}
				
		ScaleConnector sc2 = new ScaleConnector("", 0, 0, 0, 0);		
		sc2.setStartCharacter(3);
		sc2.setEndCharacter(3);		
		sc2.setReadings(3);
		
		System.out.println(sc2.getValue(String.format("%c255%c%c255%c%c999%c", sc, ec,sc, ec,sc, ec)));
		
		for (int i = 0; i < 3; i++) {
			String value = sc2.getValue(String.format("%c255%c%c255%c%c999%c", sc, ec,sc, ec,sc, ec));
			if(list.containsKey(value)){
				list.put(value, list.get(value)+1);
			}else{
				list.put(value, 1);
			}
		}

	}

}
